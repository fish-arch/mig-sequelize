# mig component sample - Sequelize

This is a normal implementation of a migrations module for a bookstore.
Please feel free to use it as reference or work on it.

For further documentation, please check the [Sequelize documentation](https://sequelize.org/v4/manual/tutorial/models-definition.html).

# Why Sequelize?

JS is fairly simple for most operations.

# Outline

- **/migrations** is the folder where migrations and it's state are stored.
- **/models** is the folder where models are stored.
- **/models/newspaper** is a rough example of a module linkage. They may be linked from other migrations modules into a single one for **the root module**.

# How to use

## 1. Set up database connection

1.1. Please run the following command:

```bash
bash init.sh
```

This will copy the configurations template `environment.json` into a non-tracked file (to avoid commiting credentials).

1.2 Edit the `environment.json` and set up your credentials for the connection.

## 2. Create Sequelize models

Please check the examples or the [Sequelize documentation](https://sequelize.org/v4/manual/tutorial/models-definition.html) for more information.

## 3. Generate migrations

They can be generated manually (for adding/deleting data), or automatically if it's adding a table/column/index.

Before generating an automatic migration, please preview the result with:

```bash
npm run am-preview
```

In order to generate a migration with the changes detected, please run:

```bash
npm run am-plan
```

Finally, to apply it, run:

```bash
npm run am-pending
```

In case you are chaining multiple migrations, please run:

```bash
bash natural-migration.sh
```

This last one is because the earlier commands will not run them in a proper order.

## 4. Additional guidelines

Please follow this naming schema, for manual migrations:

1-manual-add-books

That is:

- **1** is the revision to apply it afterwards.
- **manual** is to specify it's a manual migration.
- **add-books** is because we're going to insert books in this migration.

## TODOs:

- Must add capability of detecting and skipping already applied migrations.
- Must add migration name generation to include module name as prefix.
- Must test integration of other modules for root module.