// For docker use only
const fs = require('fs');
const path = require('path');
const { Client }  = require('pg');

const env = process.env.NODE_ENV || 'development';
const config = require(path.join(__dirname, 'config', 'config.json'))[env];

const migrationsDir = path.join(__dirname, 'migrations');
const appliedMigrationsDir = path.join(__dirname, 'applied-migrations');
if (!fs.existsSync(appliedMigrationsDir)) {
  fs.mkdirSync(appliedMigrationsDir);
}

async function prepareToRun() {
  const client = new Client({
    user: config.username,
    password: config.password,
    database: config.database,
    host: config.host,
    port: config.port
  });
  await client.connect();

  const results = await client.query(`SELECT * FROM ${config.migrationStorageTableName}`);
  
  results.rows.forEach(row => { 
    var file = row.name;
    if (fs.existsSync(path.join(migrationsDir, file))) {
      fs.renameSync(path.join(migrationsDir, file), path.join(appliedMigrationsDir, file))
    }
  });

  process.exit(0);
}

prepareToRun();

