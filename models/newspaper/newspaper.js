'use strict';
var Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  let prefix = 'n_';
  const newspaper = sequelize.define(
    prefix + 'newspaper', {
      newspaper_id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      name: DataTypes.STRING(120),
      company_id: DataTypes.STRING(64),
      date: DataTypes.DATE,
      active: DataTypes.BOOLEAN,
    }, {
      indexes: [
        {
          name: 'newspaper_company_date_uix',
          unique: true,
          using: 'BTREE',
          fields: ['company_id', 'date'],
        }
      ],
      freezeTableName: true,
      underscored: true
    }
  );
  newspaper.associate = function (models) {
    // associations can be defined here
    if (models['c_user']) {
      // if the core module is present, these associations will be created
      newspaper.belongsTo(models['c_user'], {
        foreignKey: 'created_by'
      });
      newspaper.belongsTo(models['c_user'], {
        foreignKey: 'updated_by'
      });
    }
  };
  return newspaper;
};