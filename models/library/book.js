'use strict';
var Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  let prefix = 'l_';
  const book = sequelize.define(
    prefix + 'book', {
      book_id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      name: DataTypes.STRING(120),
      isbn: DataTypes.STRING(64),
      active: DataTypes.BOOLEAN,
      blocked: DataTypes.BOOLEAN,
      last_time_login: DataTypes.DATE,
    }, {
      indexes: [
        {
          name: 'book_isbn_uix',
          unique: true,
          using: 'BTREE',
          fields: ['isbn'],
        }
      ],
      freezeTableName: true,
      underscored: true
    }
  );
  book.associate = function (models) {
    // associations can be defined here
    if (models['c_user']) {
      // if the core module is present, these associations will be created
      book.belongsTo(models['c_user'], {
        foreignKey: 'created_by'
      });
      book.belongsTo(models['c_user'], {
        foreignKey: 'updated_by'
      });
    }
  };
  return book;
};