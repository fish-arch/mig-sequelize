'use strict';
var Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  let prefix = 'c_';
  const user = sequelize.define(
    prefix + 'user', {
      user_id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      email_address: DataTypes.STRING(260),
      password: DataTypes.STRING(64),
      active: DataTypes.BOOLEAN,
    }, {
      indexes: [
        {
          name: 'user_email_address_uix',
          unique: true,
          using: 'BTREE',
          fields: ['email_address'],
        }
      ],
      freezeTableName: true,
      underscored: true
    }
  );
  user.associate = function (models) {
    // associations can be defined here
    user.belongsTo(models[prefix + 'user'], {
      foreignKey: 'created_by' // for audit log (?), any other type can also create a pseudo-user (?)
    });
    user.belongsTo(models[prefix + 'user'], {
      foreignKey: 'updated_by' // for audit log (?), any other type can also create a pseudo-user (?)
    });
  };
  return user;
};