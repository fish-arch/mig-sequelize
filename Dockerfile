FROM node:12-alpine3.11 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY package*.json ./
RUN npm install

# Copy everything else and build
COPY . ./

# Build runtime image #
FROM postgres:11-alpine

WORKDIR /app

# Add base image tools
RUN apk add --no-cache gettext su-exec

# Copy node.js runtimes
COPY --from=build-env /usr/local/bin/node /usr/local/bin/node
COPY --from=build-env /usr/local/lib/node_modules /usr/local/lib/node_modules
RUN ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm && \
    ln -s /usr/local/lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx && \
    ln -s /usr/local/bin/nodejs /usr/local/bin/nodejs

# Copy build results
COPY --from=build-env /app/ .
RUN chmod +x /app/docker-run-am-pending.sh

# ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh", "/app/docker-run-am-pending.sh"]

#ENTRYPOINT ["docker-entrypoint.sh"]

#CMD ["sh", "docker-run-am-pending.sh"]
# "exec", "su-exec", "postgres", "postgres", "&&", "killall postgres"