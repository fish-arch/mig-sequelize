#!/bin/bash

# Helper script for properly ordered applying of migrations (pending to detect existing ones)
# Author: José Francisco Martínez Rangel

set -e # stop on error

if [ ! -e "config/config.json" ]; then envsubst < config/config.json.template > config/config.json; fi

node prepare-to-run.js

if [ -d migrations-pending ]; then
    echo "Restarting failed migration"
    cp migrations-pending/* migrations || echo "Empty migrations-pending folder? (deleting it)"
    rm -r migrations-pending
fi

mkdir -p migrations-pending
if [ ! -z "$(ls -A migrations | grep .js$)" ]; then mv migrations/*.js migrations-pending; fi

for file in `ls migrations-pending | grep .js$ | sort -n`; do
    mv "migrations-pending/$(basename $file)" "migrations/"
    echo "Running $(basename $file)"
    npm run am-pending
    mv "migrations/$(basename $file)" "applied-migrations/$(basename $file)"
done

if [ ! -z "$(ls -A migrations-pending | grep .js$)" ]; then cp migrations-pending/* migrations; fi
if [ ! -z "$(ls -A applied-migrations | grep .js$)" ]; then cp applied-migrations/* migrations; fi
rm -r migrations-pending applied-migrations