const fs = require('fs');
const path = require('path');

module.exports = function() {
  var migrationsPath = path.join(__dirname, 'migrations');
  if (fs.lstatSync(migrationsPath).isSymbolicLink()) {
    return path.basename(fs.readlinkSync(migrationsPath)).substr('migrations-'.length);
  }
  return 'default';
}