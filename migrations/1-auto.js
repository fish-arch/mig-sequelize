'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "c_user", deps: [c_user, c_user]
 * createTable "l_book", deps: [c_user, c_user]
 * createTable "n_newspaper", deps: [c_user, c_user]
 * addIndex "user_email_address_uix" to table "c_user"
 * addIndex "book_isbn_uix" to table "l_book"
 * addIndex "newspaper_company_date_uix" to table "n_newspaper"
 *
 **/

var info = {
    "revision": 1,
    "name": "auto",
    "created": "2020-01-21T20:06:04.238Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "createTable",
        params: [
            "c_user",
            {
                "user_id": {
                    "type": Sequelize.BIGINT,
                    "field": "user_id",
                    "primaryKey": true,
                    "allowNull": false,
                    "autoIncrement": true
                },
                "email_address": {
                    "type": Sequelize.STRING(260),
                    "field": "email_address"
                },
                "password": {
                    "type": Sequelize.STRING(64),
                    "field": "password"
                },
                "active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "active"
                },
                "created_at": {
                    "type": Sequelize.DATE,
                    "field": "created_at",
                    "allowNull": false
                },
                "updated_at": {
                    "type": Sequelize.DATE,
                    "field": "updated_at",
                    "allowNull": false
                },
                "created_by": {
                    "type": Sequelize.BIGINT,
                    "field": "created_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                },
                "updated_by": {
                    "type": Sequelize.BIGINT,
                    "field": "updated_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                }
            },
            {}
        ]
    },
    {
        fn: "createTable",
        params: [
            "l_book",
            {
                "book_id": {
                    "type": Sequelize.BIGINT,
                    "field": "book_id",
                    "primaryKey": true,
                    "allowNull": false,
                    "autoIncrement": true
                },
                "name": {
                    "type": Sequelize.STRING(120),
                    "field": "name"
                },
                "isbn": {
                    "type": Sequelize.STRING(64),
                    "field": "isbn"
                },
                "active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "active"
                },
                "blocked": {
                    "type": Sequelize.BOOLEAN,
                    "field": "blocked"
                },
                "last_time_login": {
                    "type": Sequelize.DATE,
                    "field": "last_time_login"
                },
                "created_at": {
                    "type": Sequelize.DATE,
                    "field": "created_at",
                    "allowNull": false
                },
                "updated_at": {
                    "type": Sequelize.DATE,
                    "field": "updated_at",
                    "allowNull": false
                },
                "created_by": {
                    "type": Sequelize.BIGINT,
                    "field": "created_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                },
                "updated_by": {
                    "type": Sequelize.BIGINT,
                    "field": "updated_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                }
            },
            {}
        ]
    },
    {
        fn: "createTable",
        params: [
            "n_newspaper",
            {
                "newspaper_id": {
                    "type": Sequelize.BIGINT,
                    "field": "newspaper_id",
                    "primaryKey": true,
                    "allowNull": false,
                    "autoIncrement": true
                },
                "name": {
                    "type": Sequelize.STRING(120),
                    "field": "name"
                },
                "company_id": {
                    "type": Sequelize.STRING(64),
                    "field": "company_id"
                },
                "date": {
                    "type": Sequelize.DATE,
                    "field": "date"
                },
                "active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "active"
                },
                "created_at": {
                    "type": Sequelize.DATE,
                    "field": "created_at",
                    "allowNull": false
                },
                "updated_at": {
                    "type": Sequelize.DATE,
                    "field": "updated_at",
                    "allowNull": false
                },
                "created_by": {
                    "type": Sequelize.BIGINT,
                    "field": "created_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                },
                "updated_by": {
                    "type": Sequelize.BIGINT,
                    "field": "updated_by",
                    "onUpdate": "CASCADE",
                    "onDelete": "SET NULL",
                    "references": {
                        "model": "c_user",
                        "key": "user_id"
                    },
                    "allowNull": true
                }
            },
            {}
        ]
    },
    {
        fn: "addIndex",
        params: [
            "c_user",
            ["email_address"],
            {
                "indexName": "user_email_address_uix",
                "indicesType": "UNIQUE"
            }
        ]
    },
    {
        fn: "addIndex",
        params: [
            "l_book",
            ["isbn"],
            {
                "indexName": "book_isbn_uix",
                "indicesType": "UNIQUE"
            }
        ]
    },
    {
        fn: "addIndex",
        params: [
            "n_newspaper",
            ["company_id", "date"],
            {
                "indexName": "newspaper_company_date_uix",
                "indicesType": "UNIQUE"
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
