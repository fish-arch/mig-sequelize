#!/bin/sh

## setup and start PostgreSQL server
docker-entrypoint.sh postgres | (

  ## wait for server to be ready
  sleep 5 &&

  ## apply migrations
  bash natural-migrations.sh &&

  ## stop PostgreSQL server gracefully
  # For Ubuntu based images
  #runuser -l postgres -c '/usr/lib/postgresql/12/bin/pg_ctl stop -m smart'

  # For Alpine based images
  su-exec postgres /usr/local/bin/pg_ctl stop -m smart
)

exit 0