#!/bin/sh
if [[ -L 'migrations' || ! -e 'migrations' ]]; then
  if [[ -d migrations-$1 ]]; then
    rm -f migrations
    ln -s migrations-$1 migrations
  else
    echo -e "\033[0;31mERROR:\033[0;37m"
    echo -e "Migration environment \033[0;31m'$1'\033[0;37m not found"
    exit 1
  fi
else
  echo "'migrations' cannot be a folder in order to use this function (will be replaced by a symlink)"
fi