#!/bin/bash

#TODO: Use env vars and prompt to fill up

# check if environment file exists
FILE=config/config.json
if [ -f "$FILE" ]; then
  echo "$FILE exists"
else
  cp $FILE.template $FILE
  echo "Created $FILE file"
fi